#[derive(Debug, Clone, Copy)]
pub enum PinMode {
    Input,
    Output,
    PwmOutput,
    GpioClock,
    SoftPwmOutput,
    SoftToneOutput,
    PwmToneOutput
}

#[derive(Debug, Clone, Copy)]
pub enum PinPullUpDown {
    Off,
    Up,
    Down
}

#[derive(Debug)]
pub enum Pin {
    Physical(u16),
    Gpio(i32)
}

#[derive(Debug, Clone, Copy)]
pub enum Error {
    InvalidAddress,
    OutOfBounds,
    InvalidPin,
    InvalidPinModeValue(u32),
    InvalidPinMode(PinMode)
}

pub trait Gpio {
    fn normalize_pin(pin: Pin) -> Option<i32>;

    fn get_gpio_mode_raw(&self, pin: i32) -> Result<PinMode, Error>;
    fn set_gpio_mode_raw(&mut self, pin: i32, mode: PinMode) -> Result<(), Error>;
    fn digital_read_raw(&self, pin: i32) -> Result<bool, Error>;
    fn digital_write_raw(&mut self, pin: i32, value: bool) -> Result<(), Error>;
    fn set_pull_up_down_control_raw(&mut self, pin: i32, pud: PinPullUpDown) -> Result<(), Error>;

    fn get_gpio_mode(&self, pin: Pin) -> Result<PinMode, Error> {
        self.get_gpio_mode_raw(<Self>::normalize_pin(pin).ok_or(Error::InvalidPin)?)
    }

    fn set_gpio_mode(&mut self, pin: Pin, mode: PinMode) -> Result<(), Error> {
        self.set_gpio_mode_raw(<Self>::normalize_pin(pin).ok_or(Error::InvalidPin)?, mode)
    }

    fn digital_read(&self, pin: Pin) -> Result<bool, Error> {
        self.digital_read_raw(<Self>::normalize_pin(pin).ok_or(Error::InvalidPin)?)
    }

    fn digital_write(&mut self, pin: Pin, value: bool) -> Result<(), Error> {
        self.digital_write_raw(<Self>::normalize_pin(pin).ok_or(Error::InvalidPin)?, value)
    }

    fn set_pull_up_down_control(&mut self, pin: Pin, pud: PinPullUpDown) -> Result<(), Error> {
        self.set_pull_up_down_control_raw(<Self>::normalize_pin(pin).ok_or(Error::InvalidPin)?, pud)
    }
}