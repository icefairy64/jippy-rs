#[derive(Debug, Clone, Copy)]
pub enum Error {
    IoError
}

pub trait Channel {
    fn rw_data(&mut self, data: &mut [u8]) -> Result<(), Error>;

    fn write(&mut self, data: &[u8]) -> Result<(), Error> {
        let mut rx_buf = Vec::from(data);
        self.rw_data(rx_buf.as_mut_slice())
    }
}