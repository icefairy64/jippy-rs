pub mod gpio;
pub mod spi;

#[cfg(feature = "opi5")]
pub mod opi5;

#[cfg(feature = "opi5")]
#[derive(Debug)]
pub enum Opi5GpioError {
    FileOpenFailed,
    Rk3588Error(opi5::Rk3588SocError)
}

#[cfg(feature = "opi5")]
pub fn create_opi5_gpio<'a>() -> Result<opi5::Rk3588Soc<'a>, Opi5GpioError> {
    unsafe {
        let path = std::ffi::CString::new("/dev/mem").map_err(|_| Opi5GpioError::FileOpenFailed)?;
        let fd = libc::open(path.as_ptr(), libc::O_RDWR | libc::O_SYNC | libc::O_CLOEXEC);
        if fd >= 0 {
            opi5::Rk3588Soc::new(fd).map_err(Opi5GpioError::Rk3588Error)
        } else {
            Err(Opi5GpioError::FileOpenFailed)
        }
    }
}
