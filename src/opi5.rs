use crate::gpio::{Gpio, self};

const RK3588_GPIO0_BASE: libc::off_t = 0xfd8a0000i64;
const RK3588_GPIO1_BASE: libc::off_t = 0xfec20000i64;
const RK3588_GPIO2_BASE: libc::off_t = 0xfec30000i64;
const RK3588_GPIO3_BASE: libc::off_t = 0xfec40000i64;
const RK3588_GPIO4_BASE: libc::off_t = 0xfec50000i64;

// Offsets are in 4 bytes
const RK3588_GPIO_SWPORT_DR_L_OFFSET : usize = 0x00usize;
const RK3588_GPIO_SWPORT_DDR_L_OFFSET: usize = 0x02usize;
const RK3588_GPIO_EXT_PORT_OFFSET    : usize = 0x1cusize;

const RK3588_CRU_BASE: libc::off_t = 0xfd7c0000i64;
const RK3588_CRU_GATE_CON16_OFFSET: usize = 0x0210usize;
const RK3588_CRU_GATE_CON17_OFFSET: usize = 0x0211usize;

const RK3588_PMU1CRU_BASE: libc::off_t = 0xfd7f0000i64;
const RK3588_PMU1CRU_GATE_CON5_OFFSET: usize = 0x0205usize;

const RK3588_PMU1_IOC_BASE: libc::off_t = 0xfd5f0000i64;


const RK3588_PMU2_IOC_BASE: libc::off_t = 0xfd5f4000i64;

const RK3588_BUS_IOC_BASE: libc::off_t = 0xfd5f8000i64;

// Pull up/down

const RK3588_VCCIO1_4_IOC_BASE: libc::off_t = 0xfd5f9000i64;
const RK3588_VCCIO3_5_IOC_BASE: libc::off_t = 0xfd5fa000i64;
const RK3588_VCCIO6_IOC_BASE:   libc::off_t = 0xfd5fc000i64;

const RK3588_PMU1_IOC_GPIO0A_P    : usize = 0x0008;
const RK3588_PMU2_IOC_GPIO0B_P    : usize = 0x000a;
const RK3588_VCCIO1_4_IOC_GPIO1A_P: usize = 0x0027;
const RK3588_VCCIO3_5_IOC_GPIO2A_P: usize = 0x0030;
const RK3588_VCCIO6_IOC_GPIO4A_P  : usize = 0x0048;

const ORANGE_PI_PIN_MASK: [[i32; 32]; 5] = [
    [-1, -1, -1, -1, -1, -1, -1, -1,  -1, -1, -1, -1, -1, -1, -1, -1,  -1, -1, -1, -1, -1, -1, -1, -1,  -1, -1, -1, -1,  4,  5, -1, -1, ],
	[-1, -1, -1,  3, -1, -1, -1, -1,  -1, -1, -1, -1, -1, -1,  6,  7,   0,  1,  2, -1,  4, -1,  6, -1,  -1, -1,  2,  3, -1, -1, -1, -1, ],
	[-1, -1, -1, -1, -1, -1, -1, -1,  -1, -1, -1, -1, -1, -1, -1, -1,  -1, -1, -1, -1, -1, -1, -1, -1,  -1, -1, -1, -1,  4, -1, -1, -1, ],
	[-1, -1, -1, -1, -1, -1, -1, -1,  -1, -1, -1, -1, -1, -1, -1, -1,  -1, -1, -1, -1, -1, -1, -1, -1,  -1, -1, -1, -1, -1, -1, -1, -1, ],
	[-1, -1, -1,  3,  4, -1, -1, -1,  -1, -1,  2,  3,  4, -1, -1, -1,  -1, -1, -1, -1, -1, -1, -1, -1,  -1, -1, -1, -1, -1, -1, -1, -1, ]
];

const PHYS_TO_GPIO: [i32; 64] = [
    -1,
	-1, -1,
	47, -1,
	46, -1,
	54, 131,
	-1, 132,
	138, 29,
	139, -1,
	28, 59,
	-1, 58,
	49, -1,
	48, 92,
	50, 52,
	-1, 35,
	-1, -1,
	-1, -1,
	-1, -1,
	-1, -1,
	-1, -1,
	-1, -1,
	-1, -1,

    -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    -1, -1, -1, -1, -1, -1, -1
];

const BLOCK_SIZE: libc::size_t = 4 * 1024;

fn rk3588_pin_bank_index(pin: i32) -> (u32, u32) {
    let bank = pin >> 5;
    (bank as u32, (pin - (bank << 5)) as u32)
}

fn rk3588_has_pin(bank: u32, index: u32) -> bool {
    if bank > 5 || index > 32 {
        false
    } else {
        ORANGE_PI_PIN_MASK[bank as usize][index as usize] != -1
    }
}

#[derive(Debug)]
pub enum Rk3588SocError {
    MapFailed,
    InvalidAddress,
    InvalidBankOrIndex(u32, u32),
    OutOfBounds,
    InvalidPin,
    InvalidPinModeValue(u32),
    InvalidPinMode(gpio::PinMode),
    RegStatusCheckFailed(u32, u32)
}

pub fn rk3588_normalize_pin(pin: gpio::Pin) -> Option<i32> {
    let result = match pin {
        gpio::Pin::Physical(phys) => PHYS_TO_GPIO[phys as usize],
        gpio::Pin::Gpio(gpio) => gpio
    };
    if result >= 0 {
        Some(result)
    } else {
        None
    }
}

pub struct Rk3588Soc<'a> {
    fd: libc::c_int,

    gpio0: &'a mut [u32],
    gpio1: &'a mut [u32],
    gpio2: &'a mut [u32],
    gpio3: &'a mut [u32],
    gpio4: &'a mut [u32],

    pmu1_ioc:    &'a mut [u32],
    pmu2_ioc:    &'a mut [u32],
    bus_ioc:     &'a mut [u32],
    cur_ioc:     &'a mut [u32],
    pmu1cur_ioc: &'a mut [u32],

    vccio1_4: &'a mut [u32],
    vccio3_5: &'a mut [u32],
    vccio6  : &'a mut [u32],
}

fn gpio_pin_mode_from_int(value: u32) -> Option<gpio::PinMode> {
    match value {
        0 => Some(gpio::PinMode::Input),
        1 => Some(gpio::PinMode::Output),
        2 => Some(gpio::PinMode::PwmOutput),
        3 => Some(gpio::PinMode::GpioClock),
        4 => Some(gpio::PinMode::SoftPwmOutput),
        5 => Some(gpio::PinMode::SoftToneOutput),
        6 => Some(gpio::PinMode::PwmToneOutput),
        _ => None
    }
}

fn rk3588_reg_update_bits<const W: u8>(reg: u32, offset: u32, value: u32) -> u32 {
    let mask: u32 = match W {
        0 => 0b00000000,
        1 => 0b00000001,
        2 => 0b00000011,
        3 => 0b00000111,
        4 => 0b00001111,
        5 => 0b00011111,
        6 => 0b00111111,
        7 => 0b01111111,
        8 => 0b11111111,
        _ => panic!("Invalid bits width")
    };
    let mut new_reg = reg;
    new_reg |= mask << (16 + offset);
    new_reg &= !(mask << offset);
    new_reg | (value << offset)
}

impl <'a> Rk3588Soc<'a> {
    pub fn new(fd: libc::c_int) -> Result<Rk3588Soc<'a>, Rk3588SocError> {
        unsafe {
            let gpio0 = libc::mmap(std::ptr::null_mut(), BLOCK_SIZE,
                libc::PROT_READ | libc::PROT_WRITE, libc::MAP_SHARED, fd, RK3588_GPIO0_BASE);
            if gpio0 == libc::MAP_FAILED {
                return Err(Rk3588SocError::MapFailed)
            }

            let gpio1 = libc::mmap(std::ptr::null_mut(), BLOCK_SIZE,
                libc::PROT_READ | libc::PROT_WRITE, libc::MAP_SHARED, fd, RK3588_GPIO1_BASE);
            if gpio1 == libc::MAP_FAILED {
                return Err(Rk3588SocError::MapFailed)
            }

            let gpio2 = libc::mmap(std::ptr::null_mut(), BLOCK_SIZE,
                libc::PROT_READ | libc::PROT_WRITE, libc::MAP_SHARED, fd, RK3588_GPIO2_BASE);
            if gpio2 == libc::MAP_FAILED {
                return Err(Rk3588SocError::MapFailed)
            }

            let gpio3 = libc::mmap(std::ptr::null_mut(), BLOCK_SIZE,
                libc::PROT_READ | libc::PROT_WRITE, libc::MAP_SHARED, fd, RK3588_GPIO3_BASE);
            if gpio3 == libc::MAP_FAILED {
                return Err(Rk3588SocError::MapFailed)
            }

            let gpio4 = libc::mmap(std::ptr::null_mut(), BLOCK_SIZE,
                libc::PROT_READ | libc::PROT_WRITE, libc::MAP_SHARED, fd, RK3588_GPIO4_BASE);
            if gpio4 == libc::MAP_FAILED {
                return Err(Rk3588SocError::MapFailed)
            }

            let pmu1_ioc = libc::mmap(std::ptr::null_mut(), BLOCK_SIZE,
                libc::PROT_READ | libc::PROT_WRITE, libc::MAP_SHARED, fd, RK3588_PMU1_IOC_BASE);
            if pmu1_ioc == libc::MAP_FAILED {
                return Err(Rk3588SocError::MapFailed)
            }

            let pmu2_ioc = libc::mmap(std::ptr::null_mut(), BLOCK_SIZE,
                libc::PROT_READ | libc::PROT_WRITE, libc::MAP_SHARED, fd, RK3588_PMU2_IOC_BASE);
            if pmu2_ioc == libc::MAP_FAILED {
                return Err(Rk3588SocError::MapFailed)
            }

            let bus_ioc = libc::mmap(std::ptr::null_mut(), BLOCK_SIZE,
                libc::PROT_READ | libc::PROT_WRITE, libc::MAP_SHARED, fd, RK3588_BUS_IOC_BASE);
            if bus_ioc == libc::MAP_FAILED {
                return Err(Rk3588SocError::MapFailed)
            }

            let cur = libc::mmap(std::ptr::null_mut(), BLOCK_SIZE,
                libc::PROT_READ | libc::PROT_WRITE, libc::MAP_SHARED, fd, RK3588_CRU_BASE);
            if cur == libc::MAP_FAILED {
                return Err(Rk3588SocError::MapFailed)
            }

            let pmu1_cur = libc::mmap(std::ptr::null_mut(), BLOCK_SIZE,
                libc::PROT_READ | libc::PROT_WRITE, libc::MAP_SHARED, fd, RK3588_PMU1CRU_BASE);
            if pmu1_cur == libc::MAP_FAILED {
                return Err(Rk3588SocError::MapFailed)
            }

            let vccio1_4 = libc::mmap(std::ptr::null_mut(), BLOCK_SIZE,
                libc::PROT_READ | libc::PROT_WRITE, libc::MAP_SHARED, fd, RK3588_VCCIO1_4_IOC_BASE);
            if vccio1_4 == libc::MAP_FAILED {
                return Err(Rk3588SocError::MapFailed)
            }

            let vccio3_5 = libc::mmap(std::ptr::null_mut(), BLOCK_SIZE,
                libc::PROT_READ | libc::PROT_WRITE, libc::MAP_SHARED, fd, RK3588_VCCIO3_5_IOC_BASE);
            if vccio3_5 == libc::MAP_FAILED {
                return Err(Rk3588SocError::MapFailed)
            }

            let vccio6 = libc::mmap(std::ptr::null_mut(), BLOCK_SIZE,
                libc::PROT_READ | libc::PROT_WRITE, libc::MAP_SHARED, fd, RK3588_VCCIO6_IOC_BASE);
            if vccio6 == libc::MAP_FAILED {
                return Err(Rk3588SocError::MapFailed)
            }

            Ok(Rk3588Soc {
                fd,
                gpio0:       std::slice::from_raw_parts_mut::<'a>(gpio0.cast::<u32>(),    BLOCK_SIZE),
                gpio1:       std::slice::from_raw_parts_mut::<'a>(gpio1.cast::<u32>(),    BLOCK_SIZE),
                gpio2:       std::slice::from_raw_parts_mut::<'a>(gpio2.cast::<u32>(),    BLOCK_SIZE),
                gpio3:       std::slice::from_raw_parts_mut::<'a>(gpio3.cast::<u32>(),    BLOCK_SIZE),
                gpio4:       std::slice::from_raw_parts_mut::<'a>(gpio4.cast::<u32>(),    BLOCK_SIZE),
                pmu1_ioc:    std::slice::from_raw_parts_mut::<'a>(pmu1_ioc.cast::<u32>(), BLOCK_SIZE),
                pmu2_ioc:    std::slice::from_raw_parts_mut::<'a>(pmu2_ioc.cast::<u32>(), BLOCK_SIZE),
                bus_ioc:     std::slice::from_raw_parts_mut::<'a>(bus_ioc.cast::<u32>(),  BLOCK_SIZE),
                cur_ioc:     std::slice::from_raw_parts_mut::<'a>(cur.cast::<u32>(),      BLOCK_SIZE),
                pmu1cur_ioc: std::slice::from_raw_parts_mut::<'a>(pmu1_cur.cast::<u32>(), BLOCK_SIZE),
                vccio1_4:    std::slice::from_raw_parts_mut::<'a>(vccio1_4.cast::<u32>(), BLOCK_SIZE),
                vccio3_5:    std::slice::from_raw_parts_mut::<'a>(vccio3_5.cast::<u32>(), BLOCK_SIZE),
                vccio6:      std::slice::from_raw_parts_mut::<'a>(vccio6.cast::<u32>(),   BLOCK_SIZE),
            })
        }
    }

    fn mut_pin_ref(&mut self, addr: usize) -> Result<&mut u32, Rk3588SocError> {
        let mmap_base = addr & 0xfffff000;
        let mmap_seek = addr - mmap_base;

        match mmap_base as i64 {
            RK3588_GPIO0_BASE => self.gpio0.get_mut(mmap_seek),
            RK3588_GPIO1_BASE => self.gpio1.get_mut(mmap_seek),
            RK3588_GPIO2_BASE => self.gpio2.get_mut(mmap_seek),
            RK3588_GPIO3_BASE => self.gpio3.get_mut(mmap_seek),
            RK3588_GPIO4_BASE => self.gpio4.get_mut(mmap_seek),

            RK3588_PMU1_IOC_BASE => self.pmu1_ioc.get_mut(mmap_seek),
            RK3588_BUS_IOC_BASE  => self.bus_ioc.get_mut(mmap_seek),
            RK3588_CRU_BASE      => self.cur_ioc.get_mut(mmap_seek),
            RK3588_PMU1CRU_BASE  => self.pmu1cur_ioc.get_mut(mmap_seek),

            _ => None
        }.ok_or(Rk3588SocError::InvalidAddress)
    }

    fn pin_ref(&self, addr: usize) -> Result<&u32, Rk3588SocError> {
        let mmap_base = addr & 0xfffff000;
        let mmap_seek = addr - mmap_base;

        match mmap_base as i64 {
            RK3588_GPIO0_BASE => self.gpio0.get(mmap_seek),
            RK3588_GPIO1_BASE => self.gpio1.get(mmap_seek),
            RK3588_GPIO2_BASE => self.gpio2.get(mmap_seek),
            RK3588_GPIO3_BASE => self.gpio3.get(mmap_seek),
            RK3588_GPIO4_BASE => self.gpio4.get(mmap_seek),

            RK3588_PMU1_IOC_BASE => self.pmu1_ioc.get(mmap_seek),
            RK3588_BUS_IOC_BASE  => self.bus_ioc.get(mmap_seek),
            RK3588_CRU_BASE      => self.cur_ioc.get(mmap_seek),
            RK3588_PMU1CRU_BASE  => self.pmu1cur_ioc.get(mmap_seek),

            _ => None
        }.ok_or(Rk3588SocError::InvalidAddress)
    }

    pub fn read(&self, addr: usize) -> Result<u32, Rk3588SocError> {
        let val = self.pin_ref(addr)?;
        Ok(*val)
    }

    pub fn write(&mut self, addr: usize, value: u32) -> Result<(), Rk3588SocError> {
        let val = self.mut_pin_ref(addr)?;
        *val = value;
        Ok(())
    }

    fn bus_ioc_phyref(&self, bank: u32, index: u32) -> Option<&u32> {
        self.bus_ioc.get((0x08 * bank + (index >> 2)) as usize)
    }

    fn mut_bus_ioc_phyref(&mut self, bank: u32, index: u32) -> Option<&mut u32> {
        self.bus_ioc.get_mut((0x08 * bank + (index >> 2)) as usize)
    }

    fn ddr_phyref(&self, bank: u32, index: u32) -> Result<&u32, Rk3588SocError> {
        match (bank, index) {
            (0, idx) if idx < 12 => None,
            (0, _) => self.gpio0.get(RK3588_GPIO_SWPORT_DDR_L_OFFSET + (index / 16) as usize),
            (1, _) => self.gpio1.get(RK3588_GPIO_SWPORT_DDR_L_OFFSET + (index / 16) as usize),
            (2, _) => self.gpio2.get(RK3588_GPIO_SWPORT_DDR_L_OFFSET + (index / 16) as usize),
            (3, _) => self.gpio3.get(RK3588_GPIO_SWPORT_DDR_L_OFFSET + (index / 16) as usize),
            (4, _) => self.gpio4.get(RK3588_GPIO_SWPORT_DDR_L_OFFSET + (index / 16) as usize),
            _ => None
        }.ok_or(Rk3588SocError::InvalidBankOrIndex(bank, index))
    }

    fn mut_ddr_phyref(&mut self, bank: u32, index: u32) -> Result<&mut u32, Rk3588SocError> {
        match (bank, index) {
            (0, idx) if idx < 12 => None,
            (0, _) => self.gpio0.get_mut(RK3588_GPIO_SWPORT_DDR_L_OFFSET + (index / 16) as usize),
            (1, _) => self.gpio1.get_mut(RK3588_GPIO_SWPORT_DDR_L_OFFSET + (index / 16) as usize),
            (2, _) => self.gpio2.get_mut(RK3588_GPIO_SWPORT_DDR_L_OFFSET + (index / 16) as usize),
            (3, _) => self.gpio3.get_mut(RK3588_GPIO_SWPORT_DDR_L_OFFSET + (index / 16) as usize),
            (4, _) => self.gpio4.get_mut(RK3588_GPIO_SWPORT_DDR_L_OFFSET + (index / 16) as usize),
            _ => None
        }.ok_or(Rk3588SocError::InvalidBankOrIndex(bank, index))
    }

    fn mut_dr_phyref(&mut self, bank: u32, index: u32) -> Result<&mut u32, Rk3588SocError> {
        match (bank, index) {
            (0, idx) if idx < 12 => None,
            (0, _) => self.gpio0.get_mut(RK3588_GPIO_SWPORT_DR_L_OFFSET + (index / 16) as usize),
            (1, _) => self.gpio1.get_mut(RK3588_GPIO_SWPORT_DR_L_OFFSET + (index / 16) as usize),
            (2, _) => self.gpio2.get_mut(RK3588_GPIO_SWPORT_DR_L_OFFSET + (index / 16) as usize),
            (3, _) => self.gpio3.get_mut(RK3588_GPIO_SWPORT_DR_L_OFFSET + (index / 16) as usize),
            (4, _) => self.gpio4.get_mut(RK3588_GPIO_SWPORT_DR_L_OFFSET + (index / 16) as usize),
            _ => None
        }.ok_or(Rk3588SocError::InvalidBankOrIndex(bank, index))
    }
    fn mut_cru_phyref(&mut self, bank: u32, index: u32) -> Result<&mut u32, Rk3588SocError> {
        match (bank, index) {
            (0, idx) if idx < 12 => None,
            (0, _) => self.pmu1cur_ioc.get_mut(RK3588_PMU1CRU_GATE_CON5_OFFSET),
            (1, _) => self.cur_ioc.get_mut(RK3588_CRU_GATE_CON16_OFFSET),
            (bnk, _) if (2..=4).contains(&bnk) => self.cur_ioc.get_mut(RK3588_CRU_GATE_CON17_OFFSET),
            _ => None
        }.ok_or(Rk3588SocError::InvalidBankOrIndex(bank, index))
    }

    pub fn get_gpio_mode(&self, pin: i32) -> Result<gpio::PinMode, Rk3588SocError> {
        let (bank, index) = rk3588_pin_bank_index(pin);
        if rk3588_has_pin(bank, index) {
            if bank == 0 && index < 12 {
                Err(Rk3588SocError::InvalidBankOrIndex(bank, index))
            } else {
                let bus_ioc_phy = self.bus_ioc_phyref(bank, index).ok_or(Rk3588SocError::OutOfBounds)?;
                let mode = (*bus_ioc_phy >> ((index % 4) << 2)) & 0xf;
                if mode == 0 {
                    let ddr_phy = self.ddr_phyref(bank, index)?;
                    let mode = *ddr_phy >> (index % 16) & 1;
                    gpio_pin_mode_from_int(mode).ok_or(Rk3588SocError::InvalidPinModeValue(mode))
                } else {
                    gpio_pin_mode_from_int(mode + 1).ok_or(Rk3588SocError::InvalidPinModeValue(mode + 1))
                }
            }
        } else {
            Err(Rk3588SocError::InvalidPin)
        }
    }

    pub fn set_gpio_mode(&mut self, pin: i32, mode: gpio::PinMode) -> Result<(), Rk3588SocError> {
        let (bank, index) = rk3588_pin_bank_index(pin);

        // GPIO CRU register mask to enable clocks
        let cru_val: u32 = match (bank, index) {
            (0, idx) if idx < 12 => None,
            (0, _) => Some(0xffff9fff),
            (1, _) => Some(0xffff3fff),
            (2, _) => Some(0xffffffc0),
            (3, _) => Some(0xffffffc0),
            (4, _) => Some(0xffffffc0),
            _ => None
        }.ok_or(Rk3588SocError::InvalidBankOrIndex(bank, index))?;

        if rk3588_has_pin(bank, index) {
            {
                let cru_reg = self.mut_cru_phyref(bank, index)?;
                let mut val = *cru_reg;
                val &= cru_val;
                *cru_reg = val;
            }
            
            {
                // Enable GPIO through IOMUX
                let bus_reg = self.mut_bus_ioc_phyref(bank, index).ok_or(Rk3588SocError::InvalidPin)?;
                let val = *bus_reg;
                *bus_reg = rk3588_reg_update_bits::<2>(val, (index % 4) << 2, 0);
            }

            {
                // Set data direction
                let ddr_reg = self.mut_ddr_phyref(bank, index)?;
                let val = *ddr_reg;
                *ddr_reg = rk3588_reg_update_bits::<1>(val, index % 16, match mode { 
                    gpio::PinMode::Input => Ok(0),
                    gpio::PinMode::Output => Ok(1),
                    _ => Err(Rk3588SocError::InvalidPinMode(mode))
                }?);
            }
            
            Ok(())
        } else {
            Err(Rk3588SocError::InvalidPin)
        }
    }

    pub fn digital_read(&self, pin: i32) -> Result<bool, Rk3588SocError> {
        let (bank, index) = rk3588_pin_bank_index(pin);
        let slice = match bank {
            0 => Ok(&self.gpio0),
            1 => Ok(&self.gpio1),
            2 => Ok(&self.gpio2),
            3 => Ok(&self.gpio3),
            4 => Ok(&self.gpio4),
            _ => Err(Rk3588SocError::InvalidBankOrIndex(bank, index))
        }?;
        if rk3588_has_pin(bank, index) {
            let val_ref = slice.get(RK3588_GPIO_EXT_PORT_OFFSET).ok_or(Rk3588SocError::OutOfBounds)?;
            let val = (*val_ref >> index) & 1;
            Ok(val == 1)
        } else {
            Err(Rk3588SocError::InvalidPin)
        }
    }

    pub fn digital_write(&mut self, pin: i32, value: bool) -> Result<(), Rk3588SocError> {
        let (bank, index) = rk3588_pin_bank_index(pin);

        // GPIO CRU register mask to enable clocks
        let cru_val: u32 = match (bank, index) {
            (0, idx) if idx < 12 => None,
            (0, _) => Some(0xffff9fff),
            (1, _) => Some(0xffff3fff),
            (2, _) => Some(0xffffffc0),
            (3, _) => Some(0xffffffc0),
            (4, _) => Some(0xffffffc0),
            _ => None
        }.ok_or(Rk3588SocError::InvalidBankOrIndex(bank, index))?;

        if rk3588_has_pin(bank, index) {
            {
                let cru_reg = self.mut_cru_phyref(bank, index)?;
                let mut val = *cru_reg;
                val &= cru_val;
                *cru_reg = val;
                if val != *cru_reg {
                    //return Err(Rk3588SocError::RegStatusCheckFailed(val, *cru_reg));
                }
            }

            {
                let dr_reg = self.mut_dr_phyref(bank, index)?;
                let val = *dr_reg;
                *dr_reg = rk3588_reg_update_bits::<1>(val, index % 16, match value { 
                    false => 0,
                    true => 1
                });
            }
            
            Ok(())
        } else {
            Err(Rk3588SocError::InvalidPin)
        }
    }

    pub fn set_pull_up_down_control(&mut self, pin: i32, pud: gpio::PinPullUpDown) -> Result<(), Rk3588SocError> {
        let (bank, index) = rk3588_pin_bank_index(pin);

        let phy = match (bank, index) {
            (0, idx) if idx < 12 => self.pmu1_ioc.get_mut(RK3588_PMU1_IOC_GPIO0A_P + (index >> 3) as usize),
            (0, idx) if idx > 11 && idx < 31 => self.pmu2_ioc.get_mut(RK3588_PMU2_IOC_GPIO0B_P + ((index - 8) >> 3) as usize),
            (1, _) => self.vccio1_4.get_mut(RK3588_VCCIO1_4_IOC_GPIO1A_P + (index >> 3) as usize),
            (bnk, idx) if bnk < 4 || (bnk == 4 && idx > 17) => self.vccio3_5.get_mut(RK3588_VCCIO3_5_IOC_GPIO2A_P + (index >> 3) as usize),
            (4, idx) if idx < 18 => self.vccio6.get_mut(RK3588_VCCIO6_IOC_GPIO4A_P + (index >> 3) as usize),
            _ => None
        }.ok_or(Rk3588SocError::InvalidBankOrIndex(bank, index))?;

        let offset = (index % 8) << 1;

        // Actual PUD value
        // 0: PUD enable
        // 1: PUD direction
        let pud_bits = match pud {
            gpio::PinPullUpDown::Off => 0,
            gpio::PinPullUpDown::Down => 1,
            gpio::PinPullUpDown::Up => 3
        };

        if rk3588_has_pin(bank, index) {
            let val = *phy;
            *phy = rk3588_reg_update_bits::<2>(val, offset, pud_bits & 3);
            Ok(())
        } else {
            Err(Rk3588SocError::InvalidPin)
        }
    }
}

impl <'a> Drop for Rk3588Soc<'a> {
    fn drop(&mut self) {
        unsafe {
            if libc::munmap(self.gpio0.as_mut_ptr().cast::<libc::c_void>(), BLOCK_SIZE) != 0 {
                panic!("Failed to munmap");
            }
            if libc::munmap(self.gpio1.as_mut_ptr().cast::<libc::c_void>(), BLOCK_SIZE) != 0 {
                panic!("Failed to munmap");
            }
            if libc::munmap(self.gpio2.as_mut_ptr().cast::<libc::c_void>(), BLOCK_SIZE) != 0 {
                panic!("Failed to munmap");
            }
            if libc::munmap(self.gpio3.as_mut_ptr().cast::<libc::c_void>(), BLOCK_SIZE) != 0 {
                panic!("Failed to munmap");
            }
            if libc::munmap(self.gpio4.as_mut_ptr().cast::<libc::c_void>(), BLOCK_SIZE) != 0 {
                panic!("Failed to munmap");
            }
            if libc::munmap(self.pmu1_ioc.as_mut_ptr().cast::<libc::c_void>(), BLOCK_SIZE) != 0 {
                panic!("Failed to munmap");
            }
            if libc::munmap(self.pmu1cur_ioc.as_mut_ptr().cast::<libc::c_void>(), BLOCK_SIZE) != 0 {
                panic!("Failed to munmap");
            }
            if libc::munmap(self.pmu2_ioc.as_mut_ptr().cast::<libc::c_void>(), BLOCK_SIZE) != 0 {
                panic!("Failed to munmap");
            }
            if libc::munmap(self.bus_ioc.as_mut_ptr().cast::<libc::c_void>(), BLOCK_SIZE) != 0 {
                panic!("Failed to munmap");
            }
            if libc::munmap(self.cur_ioc.as_mut_ptr().cast::<libc::c_void>(), BLOCK_SIZE) != 0 {
                panic!("Failed to munmap");
            }
            if libc::munmap(self.vccio1_4.as_mut_ptr().cast::<libc::c_void>(), BLOCK_SIZE) != 0 {
                panic!("Failed to munmap");
            }
            if libc::munmap(self.vccio3_5.as_mut_ptr().cast::<libc::c_void>(), BLOCK_SIZE) != 0 {
                panic!("Failed to munmap");
            }
            if libc::munmap(self.vccio6.as_mut_ptr().cast::<libc::c_void>(), BLOCK_SIZE) != 0 {
                panic!("Failed to munmap");
            }
            if libc::close(self.fd) != 0 {
                panic!("Failed to close");
            }
        }
    }
}

// GPIO interface impl

impl From<Rk3588SocError> for gpio::Error {
    fn from(value: Rk3588SocError) -> Self {
        match value {
            Rk3588SocError::MapFailed => panic!("Should never get here"),
            Rk3588SocError::RegStatusCheckFailed(expected, actual) => panic!("Expected reg value {:08x}, got {:08x}", expected, actual),
            Rk3588SocError::InvalidAddress => gpio::Error::InvalidAddress,
            Rk3588SocError::InvalidBankOrIndex(_, _) => gpio::Error::InvalidPin,
            Rk3588SocError::OutOfBounds => gpio::Error::OutOfBounds,
            Rk3588SocError::InvalidPin => gpio::Error::InvalidPin,
            Rk3588SocError::InvalidPinModeValue(value) => gpio::Error::InvalidPinModeValue(value),
            Rk3588SocError::InvalidPinMode(mode) => gpio::Error::InvalidPinMode(mode)
        }
    }
}

impl <'a> Gpio for Rk3588Soc<'a> {
    fn normalize_pin(pin: gpio::Pin) -> Option<i32> {
        rk3588_normalize_pin(pin)
    }

    fn get_gpio_mode_raw(&self, pin: i32) -> Result<gpio::PinMode, gpio::Error> {
        self.get_gpio_mode(pin).map_err(|e| e.into())
    }

    fn set_gpio_mode_raw(&mut self, pin: i32, mode: gpio::PinMode) -> Result<(), gpio::Error> {
        self.set_gpio_mode(pin, mode).map_err(|e| e.into())
    }

    fn digital_read_raw(&self, pin: i32) -> Result<bool, gpio::Error> {
        self.digital_read(pin).map_err(|e| e.into())
    }

    fn digital_write_raw(&mut self, pin: i32, value: bool) -> Result<(), gpio::Error> {
        self.digital_write(pin, value).map_err(|e| e.into())
    }

    fn set_pull_up_down_control_raw(&mut self, pin: i32, pud: gpio::PinPullUpDown) -> Result<(), gpio::Error> {
        self.set_pull_up_down_control(pin, pud).map_err(|e| e.into())
    }
}